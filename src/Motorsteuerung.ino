/*
    Spherical POV Display Motor Control by Tim Kamenik
*/

#define EN_A           0   // PWM output/Speed (analog out)
#define IN_1           1   // direction pin 1 (digital out)
#define IN_2           4   // direction pin 2 (digital out)
#define POT           A3   // POTentiometer pin (analog in)
#define BUTTON         2   // BUTTON pin (digital in)

#define CHANGE_DELAY  20   // defines the delay for the change of the speed value

bool state = false;        // Motor is turning?
int currentPWMValue = 0;   // stores the current speed value
int targetPWMValue = 0;

void readValues();   // returns the read 10-bit analog value mapped to an 8-bit value
void setTargetPWMValue();  // sets the current target value or OFF depending on the current state

void setup()
{
  // define the function of the pins used
  pinMode(EN_A, OUTPUT);
  pinMode(IN_1, OUTPUT);
  pinMode(IN_2, OUTPUT);
  pinMode(BUTTON, INPUT_PULLUP);
  pinMode(POT, INPUT);

  // set the turning direction
  digitalWrite(IN_1, LOW);
  digitalWrite(IN_2, HIGH);
}

void loop()
{
  readValues(); // refresh the values of toggle button and potentiometer
  setTargetPWMValue(); // set PWM to the current target value or OFF  
}

void readValues()
{
  state = !digitalRead(BUTTON); // get the current state of the toggle button and invert it bc of ActiveLow circuit
  targetPWMValue = map(analogRead(POT), 0, 1023, 0, 255); // get the current analog value of the potentiometer and map it into an 8-bit range
}

void setTargetPWMValue()
{
  // if toggle button is in ON position, read and set value of potentiometer
  if (state)
  {
    while (currentPWMValue != targetPWMValue)
    {
      // increase or decrease speed value until targetvalue is reached
      if (currentPWMValue < targetPWMValue)
      {
        currentPWMValue++; // increase if current value is smaller than the target value
      } else if (currentPWMValue > targetPWMValue)
      {
        currentPWMValue--; // increase if current value is bigger than the target value
      }
      analogWrite(EN_A, currentPWMValue); // Send PWM signal to L298N Enable pin
      delay(CHANGE_DELAY); // delay next step by CHANGE_DELAY
    }
  } else // if toggle button is in OFF position, slowly turn down spped value if not already zero
  {    
    if (analogRead(EN_A) != 0)
    {
      for (int i = currentPWMValue; i >= 0; i--)
      {
        currentPWMValue = i;
        analogWrite(EN_A, currentPWMValue); // Send PWM signal to L298N Enable pin
        delay(CHANGE_DELAY);
      }
    }
  }
}

