# Spherical POV Display Motor Control

## General
This is the Motor Control program to adjust the speed of the Spherical POV Display via an H-Bridge module. It is written for the ATtiny85 with Arduino IDE.

## Circuit
![](img/Motor_circuit.png)